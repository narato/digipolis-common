﻿using Digipolis.Common.Monitoring.Models;
using System.Threading.Tasks;

namespace Digipolis.Common.Monitoring.Interfaces
{
    public interface IMonitoringService
    {
        Task<MonitoringStatus> GetMonitoringStatus();
    }
}
