﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Digipolis.Common.Monitoring.Models
{
    public class MonitoringStatus
    {
        public string Status { get; set; } // can be "ok", "warning", "error"
        public ICollection<MonitoringComponent> Components { get; set; }

        public MonitoringStatus()
        {
            Components = new List<MonitoringComponent>();
        }
    }
}
