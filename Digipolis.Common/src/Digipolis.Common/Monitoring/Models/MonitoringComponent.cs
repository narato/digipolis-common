﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Digipolis.Common.Monitoring.Models
{
    public class MonitoringComponent
    {
        public string Status { get; set; } // can be "ok", "warning", "error"
        public string Name { get; set; }
        public string Type { get; set; }
        public string Details { get; set; }
        public string ErrorMessage { get; set; }
    }
}
