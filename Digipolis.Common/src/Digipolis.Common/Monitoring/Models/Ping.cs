﻿namespace Digipolis.Common.Monitoring.Models
{
    public class Ping // simple object to return to a ping request
    {
        public string Status { get; set; }
    }
}
