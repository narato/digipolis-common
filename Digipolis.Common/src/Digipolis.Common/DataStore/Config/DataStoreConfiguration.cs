﻿namespace Digipolis.Common.DataStore.Config
{
    public class DataStoreConfiguration
    {
        public string ApiKey { get; set; } // API key for the API Management Engine to actually talk to the DataStore Engine
        public string DataTypeUrl { get; set; } // Url of the DataType engine, going through the API Management Engine
        public string DataObjectUrl { get; set; } // Url of the DataObject engine, going through the API Management Engine
        public string TenantKey { get; set; }
    }
}
