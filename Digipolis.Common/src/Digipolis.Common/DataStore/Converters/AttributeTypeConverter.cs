﻿using Digipolis.Common.DataStore.Models;
using Digipolis.Common.DataStore.Models.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;

namespace Digipolis.Common.DataStore.Converters
{
    public class AttributeTypeConverter : JsonConverter
    {
        public override bool CanWrite => false;
        public override bool CanRead => true;

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(BaseAttributeType);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject item = JObject.Load(reader);
            return Convert(item);
        }

        private GenericAttribute Convert(JToken item)
        {
            if (item["dataType"] == null)
            {
                return item.ToObject<GenericAttribute>();
            }
            // we need to do this because DataType has a list of BaseAttributeTypes, which is abstract
            var nestedAttributes = item["dataType"]["attributes"];

            item["dataType"]["attributes"].Parent.Remove();

            var returnValue = item.ToObject<GenericAttribute>();

            foreach (var nestedAttribute in nestedAttributes)
            {
                returnValue.DataType.Attributes.Add(Convert(nestedAttribute));
            }

            return returnValue;
        }

        public override void WriteJson(JsonWriter writer,
            object value, JsonSerializer serializer)
        {
            throw new InvalidOperationException("Use default serialization.");
        }
    }
}
