﻿using Digipolis.Common.DataStore.Models;
using Digipolis.Common.DataStore.Models.Conditions;
using Narato.Common.Models;
using System;
using System.Collections.Generic;

namespace Digipolis.Common.DataStore.Interfaces
{
    public interface IDataObjectClient
    {
        DataObject GetDataObjectById(Guid id);
        IEnumerable<DataObject> GetDataObjectsByIdList(IEnumerable<Guid> idList);
        PagedCollectionResponse<IEnumerable<DataObject>> GetDataObjectsByDataTypeId(Guid id, int page = 1, int pagesize = 10);
        DataObject InsertDataObject(DataObject dataObject);
        DataObject UpdateDataObject(DataObject dataObject);
        bool DeleteDataObjectById(Guid id);
        PagedCollectionResponse<IEnumerable<DataObject>> SearchDataObjectsByCondition(Guid dataTypeId, ICondition condition, int page = 1, int pagesize = 10);
        IEnumerable<ChangeSet> GetChangeSetByDataObjectId(Guid id);
    }
}
