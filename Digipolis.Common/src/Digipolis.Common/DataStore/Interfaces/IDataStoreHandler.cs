﻿using Digipolis.Common.DataStore.Models;
using Digipolis.Common.DataStore.Models.Conditions;
using Narato.Common.Models;
using System.Collections.Generic;

namespace Digipolis.Common.DataStore.Interfaces
{
    public interface IDataStoreHandler : IDataObjectClient, IDataTypeClient
    {
        PagedCollectionResponse<IEnumerable<DataObject>> GetDataObjectsByDataType(DataType dataType, int page = 1, int pagesize = 10);
        PagedCollectionResponse<IEnumerable<DataObject>> SearchDataObjectsOfDataTypeByCondition(DataType dataType, ICondition condition, int page = 1, int pagesize = 10);
    }
}
