﻿using Digipolis.Common.DataStore.Models;
using System;
using System.Collections.Generic;
using Narato.Common.Models;

namespace Digipolis.Common.DataStore.Interfaces
{
    public interface IDataTypeClient
    {
        DataType GetDataTypeById(Guid id);
        DataType GetDataTypeByName(string name);
        DataType GetDataTypeByNameOrNull(string name);
        PagedCollectionResponse<IEnumerable<DataTypeListItem>> GetDataTypeList(int page = 1, int pagesize = 10);
        DataType InsertDataType(DataType dataType);
    }
}
