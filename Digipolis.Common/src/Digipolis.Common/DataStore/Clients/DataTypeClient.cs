﻿using Digipolis.Common.DataStore.Interfaces;
using Digipolis.Common.DataStore.Models;
using Digipolis.Common.DataStore.ResponseExtensions;
using Narato.Common;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Narato.Common.Models;

namespace Digipolis.Common.DataStore.Clients
{
    public class DataTypeClient : IDataTypeClient
    {
        private readonly HttpClient _client;

        public DataTypeClient(HttpClient httpClient)
        {
            _client = httpClient;
        }

        // here we assume we ALWAYS want the tree structure
        public DataType GetDataTypeById(Guid id)
        {
            using (var response = _client.GetAsync($"api/datatypes/{id}?flatstructure=false").Result)
            {
                return response.HandleResponse<DataType>($"The datatype could not be retrieved.");
            }
        }

        /// <summary>
        /// Gets datatype by name (returns a single dataType or throws a EntityNotFoundException)
        /// </summary>
        /// <param name="name">The name of the dataType</param>
        /// <returns>The dataType, or a nice EntityNotFoundException</returns>
        public DataType GetDataTypeByName(string name)
        {
            using (var response = _client.GetAsync($"api/datatypes/name/{name}?flatstructure=false").Result)
            {
                return response.HandleResponse<DataType>($"The datatype could not be retrieved.");
            }
        }

        /// <summary>
        /// Gets datatype by name (returns a single dataType or null if not found)
        /// </summary>
        /// <param name="name">The name of the dataType</param>
        /// <returns>The dataType, or null if not found</returns>
        public DataType GetDataTypeByNameOrNull(string name)
        {
            using (var response = _client.GetAsync($"api/datatypes/name/{name}?flatstructure=false").Result)
            {
                if (response.StatusCode == HttpStatusCode.NotFound)
                {
                    return null;
                }
                return response.HandleResponse<DataType>($"The datatype could not be retrieved.");
            }
        }

        public PagedCollectionResponse<IEnumerable<DataTypeListItem>> GetDataTypeList(int page = 1, int pagesize = 10)
        {
            using (var response = _client.GetAsync($"api/datatypes?page={page}&pagesize={pagesize}").Result)
            {
                return response.HandlePagedResponse<IEnumerable<DataTypeListItem>>($"The datatypeList could not be retrieved.");
            }
        }

        public DataType InsertDataType(DataType dataType)
        {
            var content = new StringContent(dataType.ToJson());

            var request = new HttpRequestMessage(HttpMethod.Post, _client.BaseAddress + "api/datatypes");

            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            request.Content = content;

            using (var response = _client.SendAsync(request, HttpCompletionOption.ResponseContentRead).Result)
            {
                return response.HandleResponse<DataType>($"The DataType couldn't be inserted.");
            }
        }
    }
}
