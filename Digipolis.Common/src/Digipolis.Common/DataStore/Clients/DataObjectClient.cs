﻿using Digipolis.Common.DataStore.Interfaces;
using Digipolis.Common.DataStore.Models;
using Digipolis.Common.DataStore.Models.Conditions;
using Digipolis.Common.DataStore.ResponseExtensions;
using Narato.Common;
using Narato.Common.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Digipolis.Common.DataStore.Clients
{
    public class DataObjectClient : IDataObjectClient
    {
        private readonly HttpClient _client;

        public DataObjectClient(HttpClient httpClient)
        {
            _client = httpClient;
        }

        public DataObject GetDataObjectById(Guid id)
        {
            using (var response = _client.GetAsync($"api/dataobjects/{id}").Result)
            {
                return response.HandleResponse<DataObject>($"The dataObject couldn't be retrieved.");
            }
        }

        public PagedCollectionResponse<IEnumerable<DataObject>> GetDataObjectsByDataTypeId(Guid id, int page = 1, int pagesize = 10)
        {
            using (var response = _client.GetAsync($"api/datatypes/{id}/dataobjects?page={page}&pagesize={pagesize}").Result)
            {
                return response.HandlePagedResponse<IEnumerable<DataObject>>($"The DataObjects couldn't be retrieved.");
            }
        }

        public IEnumerable<DataObject> GetDataObjectsByIdList(IEnumerable<Guid> idList)
        {
            var content = new StringContent(idList.ToJson());

            var request = new HttpRequestMessage(HttpMethod.Post, _client.BaseAddress + "api/dataobjects/idList");

            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            request.Content = content;

            using (var response = _client.SendAsync(request, HttpCompletionOption.ResponseContentRead).Result)
            {
                return response.HandleResponse<IEnumerable<DataObject>>($"The DataObject couldn't be inserted.");
            }
        }

        public DataObject InsertDataObject(DataObject dataObject)
        {
            var content = new StringContent(dataObject.ToJson());

            var request = new HttpRequestMessage(HttpMethod.Post, _client.BaseAddress + "api/dataobjects");

            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            request.Content = content;

            using (var response = _client.SendAsync(request, HttpCompletionOption.ResponseContentRead).Result)
            {
                return response.HandleResponse<DataObject>($"The DataObject couldn't be inserted.");
            }
        }

        public DataObject UpdateDataObject(DataObject dataObject)
        {
            var content = new StringContent(dataObject.ToJson());

            var request = new HttpRequestMessage(HttpMethod.Put, _client.BaseAddress + "api/dataobjects/" + dataObject.Id);

            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            request.Content = content;

            using (var response = _client.SendAsync(request, HttpCompletionOption.ResponseContentRead).Result)
            {
                return response.HandleResponse<DataObject>($"The DataObject couldn't be updated.");
            }
        }

        public bool DeleteDataObjectById(Guid id)
        {
            using (var response = _client.DeleteAsync($"api/dataobjects/{id}").Result)
            {
                return response.IsSuccessStatusCode;
            }   
        }

        public PagedCollectionResponse<IEnumerable<DataObject>> SearchDataObjectsByCondition(Guid dataTypeId, ICondition condition, int page = 1, int pagesize = 10)
        {
            var content = new StringContent(condition.ToJson());

            var request = new HttpRequestMessage(HttpMethod.Post, _client.BaseAddress + "api/datatypes/" + dataTypeId + $"/dataobjects/search?page={page}&pagesize={pagesize}");

            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            request.Content = content;

            using (var response = _client.SendAsync(request, HttpCompletionOption.ResponseContentRead).Result)
            {
                return response.HandlePagedResponse<IEnumerable<DataObject>>($"The DataObject search query couldn't be completed.");
            }
        }

        public IEnumerable<ChangeSet> GetChangeSetByDataObjectId(Guid id)
        {
            using (var response = _client.GetAsync($"api/dataobjects/{id}/changesets").Result)
            {
                return response.HandleResponse<IEnumerable<ChangeSet>>($"The Changeset couldn't be retrieved.");
            }
        }
    }
}
