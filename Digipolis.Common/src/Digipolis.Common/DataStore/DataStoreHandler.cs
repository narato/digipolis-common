﻿using System;
using System.Collections.Generic;
using Digipolis.Common.DataStore.Models;
using Digipolis.Common.DataStore.Interfaces;
using Digipolis.Common.DataStore.Models.Conditions;
using Narato.Common.Models;

namespace Digipolis.Common.DataStore
{
    // integration with Digipolis' DataStore and Dossier Engine
    public class DataStoreHandler : IDataStoreHandler
    {
        private readonly IDataTypeClient _dataTypeClient;
        private readonly IDataObjectClient _dataObjectClient;

        public DataStoreHandler(IDataTypeClient dataTypeClient, IDataObjectClient dataObjectClient)
        {
            _dataObjectClient = dataObjectClient;
            _dataTypeClient = dataTypeClient;
        }

        public DataType GetDataTypeById(Guid id)
        {
            return _dataTypeClient.GetDataTypeById(id);
        }

        public PagedCollectionResponse<IEnumerable<DataTypeListItem>> GetDataTypeList(int page = 1, int pagesize = 10)
        {
            return _dataTypeClient.GetDataTypeList(page, pagesize);
        }

        public DataType InsertDataType(DataType dataType)
        {
            return _dataTypeClient.InsertDataType(dataType);
        }

        public DataObject GetDataObjectById(Guid id)
        {
            return _dataObjectClient.GetDataObjectById(id);
        }

        public IEnumerable<DataObject> GetDataObjectsByIdList(IEnumerable<Guid> idList)
        {
            return _dataObjectClient.GetDataObjectsByIdList(idList);
        }

        public PagedCollectionResponse<IEnumerable<DataObject>> GetDataObjectsByDataType(DataType dataType, int page = 1, int pagesize = 10)
        {
            return GetDataObjectsByDataTypeId(dataType.Id, page, pagesize);
        }

        public PagedCollectionResponse<IEnumerable<DataObject>> GetDataObjectsByDataTypeId(Guid id, int page = 1, int pagesize = 10)
        {
            return _dataObjectClient.GetDataObjectsByDataTypeId(id, page, pagesize);
        }

        public DataObject InsertDataObject(DataObject dataObject)
        {
            return _dataObjectClient.InsertDataObject(dataObject);
        }

        public DataObject UpdateDataObject(DataObject dataObject)
        {
            return _dataObjectClient.UpdateDataObject(dataObject);
        }

        public bool DeleteDataObjectById(Guid id)
        {
            return _dataObjectClient.DeleteDataObjectById(id);
        }

        public DataType GetDataTypeByName(string name)
        {
            return _dataTypeClient.GetDataTypeByName(name);
        }

        public DataType GetDataTypeByNameOrNull(string name)
        {
            return _dataTypeClient.GetDataTypeByNameOrNull(name);
        }

        public PagedCollectionResponse<IEnumerable<DataObject>> SearchDataObjectsOfDataTypeByCondition(DataType dataType, ICondition condition, int page = 1, int pagesize = 10)
        {
            return SearchDataObjectsByCondition(dataType.Id, condition, page, pagesize);
        }

        public PagedCollectionResponse<IEnumerable<DataObject>> SearchDataObjectsByCondition(Guid dataTypeId, ICondition condition, int page = 1, int pagesize = 10)
        {
            return _dataObjectClient.SearchDataObjectsByCondition(dataTypeId, condition, page, pagesize);
        }

        public IEnumerable<ChangeSet> GetChangeSetByDataObjectId(Guid id)
        {
            return _dataObjectClient.GetChangeSetByDataObjectId(id);
        }
    }
}
