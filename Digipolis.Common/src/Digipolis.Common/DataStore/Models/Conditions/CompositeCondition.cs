﻿using System;
using System.Collections.Generic;

namespace Digipolis.Common.DataStore.Models.Conditions
{
    public class CompositeCondition : ICondition
    {
        public const string TYPE_AND = "AND";
        public const string TYPE_OR = "OR";

        public string Operator { get; }
        public List<ICondition> Conditions { get; }

        public CompositeCondition(string op, List<ICondition> conditions)
        {
            AssertValid(op);
            Operator = op;
            Conditions = conditions ?? new List<ICondition>();
        }

        private static void AssertValid(string op)
        {
            switch (op)
            {
                case TYPE_AND:
                case TYPE_OR:
                    return;
                default:
                    throw new ArgumentException($"Invalid operator: {op}");
            }
        }
    }
}
