﻿namespace Digipolis.Common.DataStore.Models.Conditions
{
    public class Comparison : ICondition
    {
        public string Field { get; }
        public string Operator { get; }
        public string Value { get; }

        public Comparison(string field, Operator op, string value)
        {
            Field = field;
            Operator = op.Value;
            Value = value;
        }
    }
}
