﻿using System;

namespace Digipolis.Common.DataStore.Models.Conditions
{
    public class Operator
    {
        public const string EQUAL = "=";
        public const string NOT_EQUAL = "<>";
        public const string LESS_THAN = "<";
        public const string LESS_EQUAL = "<=";
        public const string GREATER_THAN = ">";
        public const string GREATER_EQUAL = ">=";
        //public const string IN = "IN";
        //public const string NOT_IN = "NIN";
        public const string CONTAINS = "CONTAINS";

        public string Value { get; }

        public Operator(string op)
        {
            AssertValid(op);
            Value = op;
        }

        private static void AssertValid(string op)
        {
            switch (op)
            {
                case EQUAL:
                case NOT_EQUAL:
                case LESS_THAN:
                case LESS_EQUAL:
                case GREATER_THAN:
                case GREATER_EQUAL:
                //case IN:
                //case NOT_IN:
                case CONTAINS:
                    return;
                default:
                    throw new ArgumentException($"Invalid operator: {op}");
            }
        }
    }
}
