﻿namespace Digipolis.Common.DataStore.Models
{
    public class ChangeSetEntry
    {
        public string Field { get; set; }
        public string OriginalValue { get; set; }
        public string NewValue { get; set; }
    }
}
