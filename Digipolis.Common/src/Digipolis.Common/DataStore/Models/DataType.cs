﻿using Digipolis.Common.DataStore.Models.Attributes;
using System;
using System.Collections.Generic;

namespace Digipolis.Common.DataStore.Models
{
    public class DataType
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<BaseAttributeType> Attributes { get; set; }

        public DataType()
        {
            Attributes = new List<BaseAttributeType>();
        }
    }
}
