﻿using System;
using System.Collections.Generic;

namespace Digipolis.Common.DataStore.Models
{
    public class ChangeSet
    {
        public DateTime Date { get; set; }
        public IEnumerable<ChangeSetEntry> Changes { get; set; }
    }
}
