﻿using Digipolis.Common.DataStore.Models.Attributes;

namespace Digipolis.Common.DataStore.Models
{
    // only used to GET dataTypes, NOT suitable for saving a dataType
    public class GenericAttribute : BaseAttributeType
    {
        public override string Type { get; set; }

        //for specific types of attributes
        public DataType DataType { get; set; }
    }
}
