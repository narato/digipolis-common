﻿namespace Digipolis.Common.DataStore.Models.Attributes
{
    public class IntAttributeType : BaseAttributeType
    {
        public override string Type
        {
            get
            {
                return "Int";
            }
            set
            {
                Type = value;
            }
        }

        public int? MinRangeValidation { get; set; }
        public int? MaxRangeValidation { get; set; }
    }
}
