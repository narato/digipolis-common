﻿namespace Digipolis.Common.DataStore.Models.Attributes
{
    public class LinkAttributeType : BaseAttributeType
    {
        public override string Type
        {
            get
            {
                return "Link";
            }
            set
            {
                Type = value;
            }
        }

        public string RegexValidation { get; }
    }
}
