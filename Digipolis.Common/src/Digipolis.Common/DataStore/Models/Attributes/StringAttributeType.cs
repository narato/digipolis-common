﻿namespace Digipolis.Common.DataStore.Models.Attributes
{
    public class StringAttributeType : BaseAttributeType
    {
        public override string Type
        {
            get
            {
                return "String";
            }
            set
            {
                Type = value;
            }
        }
        
        public string RegexValidation { get; set; }
    }
}
