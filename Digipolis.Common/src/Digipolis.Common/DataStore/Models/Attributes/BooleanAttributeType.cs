﻿namespace Digipolis.Common.DataStore.Models.Attributes
{
    public class BooleanAttributeType : BaseAttributeType
    {
        public override string Type
        {
            get
            {
                return "Boolean";
            }
            set
            {
                Type = value;
            }
        }
    }
}
