﻿using System;

namespace Digipolis.Common.DataStore.Models.Attributes
{
    public class LinkDataTypeAttributeType : BaseAttributeType
    {
        public override string Type
        {
            get
            {
                return "LinkDataType";
            }
            set
            {
                Type = value;
            }
        }

        public Guid DataTypeId { get; set; }
    }
}
