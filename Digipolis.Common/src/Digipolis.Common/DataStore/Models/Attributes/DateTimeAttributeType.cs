﻿namespace Digipolis.Common.DataStore.Models.Attributes
{
    public class DateTimeAttributeType : BaseAttributeType
    {
        public override string Type
        {
            get
            {
                return "DateTime";
            }
            set
            {
                Type = value;
            }
        }
    }
}
