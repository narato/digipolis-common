﻿namespace Digipolis.Common.DataStore.Models.Attributes
{
    public class DoubleAttributeType : BaseAttributeType
    {
        public override string Type
        {
            get
            {
                return "Double";
            }
             set
            {
                Type = value;
            }
        }

        public double? MinRangeValidation { get; set; }
        public double? MaxRangeValidation { get; set; }
    }
}
