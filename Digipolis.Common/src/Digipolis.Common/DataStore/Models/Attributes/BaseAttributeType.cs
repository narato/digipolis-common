﻿namespace Digipolis.Common.DataStore.Models.Attributes
{
    public abstract class BaseAttributeType
    {
        public abstract string Type { get; set; }

        public string Name { get; set; }

        public bool? IsRequired { get; set; }

        public object DefaultValue { get; set; }

        public bool? IsCollection { get; set; }
    }
}
