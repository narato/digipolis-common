﻿using System;
using System.Collections.Generic;

namespace Digipolis.Common.DataStore.Models
{
    public class DataObject
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string Name { get; set; }
        public Guid DataTypeId { get; set; }
        public Dictionary<string, object> Values { get; set; }

        public DataObject()
        {
            Values = new Dictionary<string, object>();
        }
    }
}
