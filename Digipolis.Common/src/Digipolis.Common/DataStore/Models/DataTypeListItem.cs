﻿using System;

namespace Digipolis.Common.DataStore.Models
{
    public class DataTypeListItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
