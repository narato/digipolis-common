﻿using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.Common.Tenancy.Models;

namespace Digipolis.Common.Tenancy.Mappers
{
    public interface IConfigMapper<ConfigT>
    {
        Tenant<ConfigT> Map(Tenant tenant);
        Tenant Map(Tenant<ConfigT> tenant);
    }
}
