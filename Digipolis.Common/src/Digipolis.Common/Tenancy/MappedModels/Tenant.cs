﻿using Digipolis.Common.Tenancy.Models;
using System;

namespace Digipolis.Common.Tenancy.MappedModels
{
    public class Tenant<ConfigT> : TenantDTO
    {
        public Guid Id { get; set; }
        public string Status { get; set; }
        public string Key { get; set; }
        public ConfigT Config { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
