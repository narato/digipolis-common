﻿namespace Digipolis.Common.Tenancy.Models
{
    public class TenantDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
