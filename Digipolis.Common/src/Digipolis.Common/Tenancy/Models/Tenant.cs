﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Digipolis.Common.Tenancy.Models
{
    public class Tenant
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string Key { get; set; }
        public string Config { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedAt { get; set; }
    }
}
