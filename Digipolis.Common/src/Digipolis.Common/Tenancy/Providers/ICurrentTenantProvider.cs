﻿using Digipolis.Common.Tenancy.MappedModels;
using System.Threading.Tasks;

namespace Digipolis.Common.Tenancy.Providers
{
    public interface ICurrentTenantProvider<ConfigT>
    {
        Task<Tenant<ConfigT>> GetCurrentTenantAsync(bool force = false);
        Tenant<ConfigT> GetCurrentTenant(bool force = false);

        string GetCurrentTenantKey();
    }
}
