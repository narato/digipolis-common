﻿namespace Digipolis.Common.Events
{
    public interface IEventhandlerPing
    {
        bool Ping();
    }
}
