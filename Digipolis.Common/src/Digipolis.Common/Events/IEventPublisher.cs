﻿
namespace Digipolis.Common.Events
{
    public interface IEventPublisher
    {
        void Publish(string ownerKey, string eventNamespace, string topic, object payload);
    }
}
