﻿using Digipolis.Common.Events.Models;

namespace Digipolis.Common.Events
{
    public interface IEventHandlerAdministrator
    {
        void EnsureTopicExists(string ownerKey, string namespaceOfTopic, string topic);

        void EnsureNamespaceExists(NamespaceConfiguration newNamespace);
    }
}
