﻿
namespace Digipolis.Common.Events.Config
{
    public class EventHandlerConfiguration
    {
        /**
         * required
         */
        public string ApiKey { get; set; } // API key for the API Management Engine to actually talk to the Event Handler Engine
        public string EventHandlerEndpoint { get; set; } // Endpoint, going through the API Management Engine
        public string AdminOwnerKey { get; set; }
    }
}
