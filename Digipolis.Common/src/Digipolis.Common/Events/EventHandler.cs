﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Digipolis.Common.Events.Models;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Serialization;
using Digipolis.Common.Events.Config;
using Narato.Common;

namespace Digipolis.Common.Events
{
    // integration with Digipolis' Event Handler Engine
    public class EventHandler : IEventPublisher, IEventHandlerAdministrator, IEventSubscriber, IEventhandlerPing
    {
        private readonly HttpClient _client;
        private readonly EventHandlerConfiguration _eventHandlerConfig;

        public EventHandler(IOptions<EventHandlerConfiguration> engineConfig, HttpClient httpClient)
        {
            _eventHandlerConfig = engineConfig.Value;
            _client = httpClient;
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            if (! _client.DefaultRequestHeaders.Contains("apikey"))
            {
                _client.DefaultRequestHeaders.Add("apikey", _eventHandlerConfig.ApiKey);
            }
        }

        private bool NamespaceExists(NamespaceConfiguration newNamespace)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, $"config/namespaces");
            request.Headers.Add("owner-key", _eventHandlerConfig.AdminOwnerKey);
            var response = _client.SendAsync(request).Result;

            HandleResponse(response);
            // if we get to below code, all errors should be handled, therefore we can assume the response was successful
            var responseString = response.Content.ReadAsStringAsync().Result;
            var namespaces = JsonConvert.DeserializeObject<NamespaceList>(responseString);

            return namespaces.Namespaces.Exists(n => n.Name == newNamespace.Namespace.Name);
        }

        private bool TopicExists(string namespaceOfTopic, string topic)
        {
            var response = _client.GetAsync($"{namespaceOfTopic}/topics").Result;

            HandleResponse(response);
            // if we get to below code, all errors should be handled, therefore we can assume the response was successful
            var responseString = response.Content.ReadAsStringAsync().Result;
            var topics = JsonConvert.DeserializeObject<TopicList>(responseString);

            return topics.Topics.Exists(t => t.Name == topic);
        }

        public void EnsureNamespaceExists(NamespaceConfiguration newNamespace)
        {
            if (NamespaceExists(newNamespace))
            {
                return;
            }
            var payloadAsJson = newNamespace.ToJson();

            var content = new StringContent(payloadAsJson);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var request = new HttpRequestMessage(HttpMethod.Post, $"config/namespaces");
            request.Headers.Add("owner-key", _eventHandlerConfig.AdminOwnerKey);
            request.Content = content;
            var response = _client.SendAsync(request).Result;

            HandleResponse(response);
        }

        public void EnsureTopicExists(string ownerKey, string namespaceOfTopic, string topic)
        {
            if (TopicExists(namespaceOfTopic, topic))
            {
                return;
            }
            var content = new StringContent("{}");
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var request = new HttpRequestMessage(HttpMethod.Put, $"{namespaceOfTopic}/topics/{topic}");
            request.Headers.Add("owner-key", ownerKey);
            request.Content = content;
            var response = _client.SendAsync(request).Result;

            HandleResponse(response);
        }

        public void Publish(string ownerKey, string eventNamespace, string topic, object payload)
        {
            var payloadAsJson = payload.ToJson();
            var content = new StringContent(payloadAsJson);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var request = new HttpRequestMessage(HttpMethod.Put, $"{eventNamespace}/{topic}/publish");
            request.Headers.Add("owner-key", ownerKey);
            request.Content = content;
            var response = _client.SendAsync(request).Result;

            HandleResponse(response);
        }

        public bool Ping()
        {
            var request = new HttpRequestMessage(HttpMethod.Get, $"ping");
            var response = _client.SendAsync(request).Result;

            HandleResponse(response);

            return true;
        }

        public void Subscribe(string eventNamespace, string nameSubscription, Subscription subscription, string subscriptionOwnerKey)
        {
            var payloadAsJson = subscription.ToJson();

            var content = new StringContent(payloadAsJson);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var request = new HttpRequestMessage(HttpMethod.Post, $"{eventNamespace}/subscriptions/{nameSubscription}");
            request.Headers.Add("owner-key", subscriptionOwnerKey == null ? nameSubscription : subscriptionOwnerKey);
            request.Content = content;

            var response = _client.SendAsync(request).Result;

            HandleResponse(response);
        }

        private void HandleResponse(HttpResponseMessage response)
        {
            if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Accepted || response.StatusCode == HttpStatusCode.NoContent)
            {
                return;
            }
            var responseString = response.Content.ReadAsStringAsync().Result;
            var errorObject = JsonConvert.DeserializeObject<ErrorMessage>(responseString);
            if (response.StatusCode == HttpStatusCode.Forbidden)
            {
                throw new UnauthorizedAccessException(errorObject.Message);
            }
            else if (response.StatusCode == HttpStatusCode.NotFound || response.StatusCode == HttpStatusCode.InternalServerError || response.StatusCode == HttpStatusCode.BadRequest)
            {
                throw new Exception(errorObject.Message);
            }

            throw new Exception($"Unknown statuscode returned: {response.StatusCode}, with message: {errorObject.Message}");
        }
    }
}
