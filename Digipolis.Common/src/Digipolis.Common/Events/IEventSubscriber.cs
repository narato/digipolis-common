﻿using Digipolis.Common.Events.Models;

namespace Digipolis.Common.Events
{
    public interface IEventSubscriber
    {
        void Subscribe(string eventNamespace, string nameSubscription, Subscription subscription, string subscriptionOwnerKey);
    }
}