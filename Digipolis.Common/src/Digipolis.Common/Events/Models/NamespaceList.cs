﻿using System.Collections.Generic;

namespace Digipolis.Common.Events.Models
{
    public class NamespaceList
    {
        public List<Namespace> Namespaces { get; set; }
    }
}