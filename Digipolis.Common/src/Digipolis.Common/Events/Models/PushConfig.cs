﻿namespace Digipolis.Common.Events.Models
{
    public class PushConfig
    {
        public string Url { get; set; }
    }
}
