﻿using System.Collections.Generic;

namespace Digipolis.Common.Events.Models
{
    public class Namespace
    {
        public string Name { get; set; }
        public string Owner { get; set; }
        public List<string> HeaderWhitelist { get; set; }
    }
}