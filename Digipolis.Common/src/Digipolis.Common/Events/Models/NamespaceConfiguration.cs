﻿
namespace Digipolis.Common.Events.Models
{
    public class NamespaceConfiguration
    {
        public Namespace Namespace { get; set; }
    }
}
