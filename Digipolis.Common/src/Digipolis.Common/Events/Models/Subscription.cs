﻿namespace Digipolis.Common.Events.Models
{
    public class Subscription
    {
        public string Topic { get; set; }
        public SubscriptionConfig Config { get; set; }
    }
}
