﻿using System.Collections.Generic;

namespace Digipolis.Common.Events.Models
{
    public class TopicList
    {
        public List<Topic> Topics { get; set; }
    }
}
