﻿
namespace Digipolis.Common.Events.Models
{
    public class Topic
    {
        public string Name { get; set; }
        public string Namespace { get; set; }
    }
}
