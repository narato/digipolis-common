﻿namespace Digipolis.Common.Events.Models
{
    public class SubscriptionConfig
    {
        public PushConfig Push { get; set; }
    }
}
