﻿using Digipolis.Common.Status.Models;
using System.Threading.Tasks;

namespace Digipolis.Common.Status.Interfaces
{
    [System.Obsolete("SystemStatusManager is deprecated, please use MonitoringManager instead.")]
    public interface ISystemStatusManager
    {
        SystemStatus GetStatus();
        Task<SystemStatus> GetStatusAsync();
    }
}
