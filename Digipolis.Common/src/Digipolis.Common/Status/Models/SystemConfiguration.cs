﻿using System;

namespace Digipolis.Common.Status.Models
{
    [Obsolete("SystemConfiguration is deprecated")]
    public class SystemConfiguration
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Version { get; set; }
        public string Environment { get; set; }
        public DateTime BuiltOn { get; set; }
    }
}
