﻿using System;
using System.Collections.Generic;

namespace Digipolis.Common.Status.Models
{
    [Obsolete("SystemStatus is deprecated, use MonitoringStatus instead")]
    public class SystemStatus
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string MoreInfo { get; set; }
        public string Version { get; set; }
        public string Environment { get; set; }
        public DateTime BuiltOn { get; set; }
        public bool Up { get; set; }
        public List<Component> Components { get; set; }


        public SystemStatus(Guid id, string name, string description, string version, string environment, DateTime builtOn)
        {
            Id = id;
            Name = name;
            Description = description;
            Version = version;
            Environment = environment;
            BuiltOn = builtOn;
            Components = new List<Component>();
        }
    }
}
