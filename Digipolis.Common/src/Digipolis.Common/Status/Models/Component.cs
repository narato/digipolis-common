﻿
namespace Digipolis.Common.Status.Models
{
    [System.Obsolete("Component is deprecated, please use MonitoringComponent instead.")]
    public class Component
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Up { get; set; }
        public string MoreInfo { get; set; }
    }
}
