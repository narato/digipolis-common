﻿using System;

namespace Digipolis.Common.Status.Models
{
    [Obsolete("DbConnectionCheckResult is deprecated")]
    public class DbConnectionCheckResult
    {
        public bool ConnectionOk { get; set; }
        public Exception Exception { get; set; }

        public DbConnectionCheckResult(bool connectionOk, Exception ex)
        {
            ConnectionOk = connectionOk;
            Exception = ex;
        }

        public DbConnectionCheckResult(bool connectionOk) :this(connectionOk, null) { }
    }
}
