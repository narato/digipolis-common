﻿using Digipolis.Common.DataStore;
using Digipolis.Common.DataStore.Clients;
using Digipolis.Common.DataStore.Config;
using Digipolis.Common.DataStore.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Digipolis.Common.DependencyInjection
{
    public static class DigipolisCommonServiceCollectionExtensions
    {
        private enum DataStoreBackends
        {
            DATATYPE,
            DATAOBJECT
        }

        public static IServiceCollection AddDataStore(this IServiceCollection services, IConfigurationRoot configuration)
        {
            services.Configure<DataStoreConfiguration>(configuration.GetSection("DataStoreConfiguration"));

            services.AddSingleton<IDataObjectClient, DataObjectClient>(c => {
                var config = c.GetService<IOptions<DataStoreConfiguration>>();
                return new DataObjectClient(GetDataStoreHttpClient(config.Value, DataStoreBackends.DATAOBJECT));
            });
            services.AddSingleton<IDataTypeClient, DataTypeClient>(c => {
                var config = c.GetService<IOptions<DataStoreConfiguration>>();
                return new DataTypeClient(GetDataStoreHttpClient(config.Value, DataStoreBackends.DATATYPE));
            });
            services.AddTransient<IDataStoreHandler, DataStoreHandler>();

            return services;

        }

        private static HttpClient GetDataStoreHttpClient(DataStoreConfiguration config, DataStoreBackends type)
        {
            var url = type == DataStoreBackends.DATATYPE ? config.DataTypeUrl : config.DataObjectUrl;
            if (url == null)
            {
                throw new ArgumentException("DataStoreConfiguration is not filled in!");
            }

            var client = new HttpClient() { BaseAddress = new Uri(url) };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            if (!client.DefaultRequestHeaders.Contains("apikey"))
            {
                client.DefaultRequestHeaders.Add("apikey", config.ApiKey);
            }
            if (!client.DefaultRequestHeaders.Contains("Tenant-Key"))
            {
                client.DefaultRequestHeaders.Add("Tenant-Key", config.TenantKey);
            }
            return client;
        }
    }
}
