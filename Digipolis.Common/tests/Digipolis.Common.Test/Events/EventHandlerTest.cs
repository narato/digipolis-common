﻿using Digipolis.Common.Events.Config;
using Digipolis.Common.Events.Models;
using Digipolis.Common.Test.Stubs;
using Microsoft.Extensions.Options;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using Xunit;

namespace Digipolis.Common.Test.Events
{
    public class EventHandlerTest
    {

        [Fact]
        public void ExistingNamespaceDoesntHaveToBeCreatedAnymore()
        {
            var existingNamespace = new Namespace()
            {
                Name = "Namespace",
                Owner = "De Papi"
            };

            var namespaceList = new NamespaceList()
            {
                Namespaces = new List<Namespace>()
            };
            namespaceList.Namespaces.Add(existingNamespace);

            var msgHandler = new Mock<FakeHttpMessageHandler>() { CallBase = true };
            var httpClient = new HttpClient(msgHandler.Object);
            httpClient.BaseAddress = new Uri("http://test/");

            var getHttpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK);
            getHttpResponseMessage.Content = new StringContent(JsonConvert.SerializeObject(namespaceList));

            var postHttpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK);
            postHttpResponseMessage.Content = new StringContent("");

            Expression<Func<HttpRequestMessage, bool>> getNamespaceRequestMessageArgument = msg =>
                 msg.Method == HttpMethod.Get &&
                 msg.RequestUri.ToString() == $"http://test/config/namespaces";

            Expression<Func<HttpRequestMessage, bool>> postNamespaceRequestMessageArgument = msg =>
                 msg.Method == HttpMethod.Post &&
                 msg.RequestUri.ToString() == $"http://test/config/namespaces";

            msgHandler.Setup(t => t.Send(It.Is(getNamespaceRequestMessageArgument)))
                    .Returns(getHttpResponseMessage);
            msgHandler.Setup(t => t.Send(It.Is(postNamespaceRequestMessageArgument)))
                    .Returns(postHttpResponseMessage);

            Mock<IOptions<EventHandlerConfiguration>> configurationOptions = new Mock<IOptions<EventHandlerConfiguration>>();
            configurationOptions.SetupGet(x => x.Value).Returns(new EventHandlerConfiguration());

            var eventHandler = new Common.Events.EventHandler(configurationOptions.Object, httpClient);

            var namespaceConfiguration = new NamespaceConfiguration()
            {
                Namespace = existingNamespace
            };
            eventHandler.EnsureNamespaceExists(namespaceConfiguration);

            // no explicit assertion, because our "assertion" is "no exception thrown"

            msgHandler.Verify(x => x.Send(It.Is(getNamespaceRequestMessageArgument)), Times.Once);
            msgHandler.Verify(x => x.Send(It.Is(postNamespaceRequestMessageArgument)), Times.Never);
        }

        [Fact]
        public void NonExistingNamespaceHasToBeCreated()
        {
            var namespaceList = new NamespaceList()
            {
                Namespaces = new List<Namespace>()
            };

            var msgHandler = new Mock<FakeHttpMessageHandler>() { CallBase = true };
            var httpClient = new HttpClient(msgHandler.Object);
            httpClient.BaseAddress = new Uri("http://test/");

            var getHttpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK);
            getHttpResponseMessage.Content = new StringContent(JsonConvert.SerializeObject(namespaceList));

            var postHttpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK);
            postHttpResponseMessage.Content = new StringContent("");

            Expression<Func<HttpRequestMessage, bool>> getNamespaceRequestMessageArgument = msg =>
                 msg.Method == HttpMethod.Get &&
                 msg.RequestUri.ToString() == $"http://test/config/namespaces";

            Expression<Func<HttpRequestMessage, bool>> postNamespaceRequestMessageArgument = msg =>
                 msg.Method == HttpMethod.Post &&
                 msg.RequestUri.ToString() == $"http://test/config/namespaces";

            msgHandler.Setup(t => t.Send(It.Is(getNamespaceRequestMessageArgument)))
                    .Returns(getHttpResponseMessage);
            msgHandler.Setup(t => t.Send(It.Is(postNamespaceRequestMessageArgument)))
                    .Returns(postHttpResponseMessage);

            Mock<IOptions<EventHandlerConfiguration>> configurationOptions = new Mock<IOptions<EventHandlerConfiguration>>();
            configurationOptions.SetupGet(x => x.Value).Returns(new EventHandlerConfiguration());

            var eventHandler = new Common.Events.EventHandler(configurationOptions.Object, httpClient);

            var namespaceConfiguration = new NamespaceConfiguration()
            {
                Namespace = new Namespace()
                {
                    Name = "Namespace",
                    Owner = "De Papi"
                }
            };
            eventHandler.EnsureNamespaceExists(namespaceConfiguration);

            // no explicit assertion, because our "assertion" is "no exception thrown"

            msgHandler.Verify(x => x.Send(It.Is(getNamespaceRequestMessageArgument)), Times.Once);
            msgHandler.Verify(x => x.Send(It.Is(postNamespaceRequestMessageArgument)), Times.Once);
        }

        [Fact]
        public void ExistingTopicDoesntHaveToBeCreatedAnymore()
        {
            var existingTopic = new Topic()
            {
                Name = "Topic",
                Namespace = "Namespace"
            };

            var topicList = new TopicList()
            {
                Topics = new List<Topic>()
            };
            topicList.Topics.Add(existingTopic);

            var msgHandler = new Mock<FakeHttpMessageHandler>() { CallBase = true };
            var httpClient = new HttpClient(msgHandler.Object);
            httpClient.BaseAddress = new Uri("http://test/");

            var getHttpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK);
            getHttpResponseMessage.Content = new StringContent(JsonConvert.SerializeObject(topicList));

            var putHttpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK);
            putHttpResponseMessage.Content = new StringContent("");

            Expression<Func<HttpRequestMessage, bool>> getTopicRequestMessageArgument = msg =>
                 msg.Method == HttpMethod.Get &&
                 msg.RequestUri.ToString() == $"http://test/{existingTopic.Namespace}/topics";

            Expression<Func<HttpRequestMessage, bool>> putTopicRequestMessageArgument = msg =>
                 msg.Method == HttpMethod.Put &&
                 msg.RequestUri.ToString() == $"http://test/{existingTopic.Namespace}/topics/{existingTopic.Name}";

            msgHandler.Setup(t => t.Send(It.Is(getTopicRequestMessageArgument)))
                    .Returns(getHttpResponseMessage);
            msgHandler.Setup(t => t.Send(It.Is(putTopicRequestMessageArgument)))
                    .Returns(putHttpResponseMessage);

            Mock<IOptions<EventHandlerConfiguration>> configurationOptions = new Mock<IOptions<EventHandlerConfiguration>>();
            configurationOptions.SetupGet(x => x.Value).Returns(new EventHandlerConfiguration());

            var eventHandler = new Common.Events.EventHandler(configurationOptions.Object, httpClient);

            eventHandler.EnsureTopicExists("me", existingTopic.Namespace, existingTopic.Name);

            // no explicit assertion, because our "assertion" is "no exception thrown"

            msgHandler.Verify(x => x.Send(It.Is(getTopicRequestMessageArgument)), Times.Once);
            msgHandler.Verify(x => x.Send(It.Is(putTopicRequestMessageArgument)), Times.Never);
        }

        [Fact]
        public void NonExistingTopicHasToBeCreated()
        {
            var topicNamespace = "namespace";
            var topicName = "topic";
            var topicList = new TopicList()
            {
                Topics = new List<Topic>()
            };

            var msgHandler = new Mock<FakeHttpMessageHandler>() { CallBase = true };
            var httpClient = new HttpClient(msgHandler.Object);
            httpClient.BaseAddress = new Uri("http://test/");

            var getHttpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK);
            getHttpResponseMessage.Content = new StringContent(JsonConvert.SerializeObject(topicList));

            var putHttpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK);
            putHttpResponseMessage.Content = new StringContent("");

            Expression<Func<HttpRequestMessage, bool>> getTopicRequestMessageArgument = msg =>
                 msg.Method == HttpMethod.Get &&
                 msg.RequestUri.ToString() == $"http://test/{topicNamespace}/topics";

            Expression<Func<HttpRequestMessage, bool>> putTopicRequestMessageArgument = msg =>
                 msg.Method == HttpMethod.Put &&
                 msg.RequestUri.ToString() == $"http://test/{topicNamespace}/topics/{topicName}";

            msgHandler.Setup(t => t.Send(It.Is(getTopicRequestMessageArgument)))
                    .Returns(getHttpResponseMessage);
            msgHandler.Setup(t => t.Send(It.Is(putTopicRequestMessageArgument)))
                    .Returns(putHttpResponseMessage);

            Mock<IOptions<EventHandlerConfiguration>> configurationOptions = new Mock<IOptions<EventHandlerConfiguration>>();
            configurationOptions.SetupGet(x => x.Value).Returns(new EventHandlerConfiguration());

            var eventHandler = new Common.Events.EventHandler(configurationOptions.Object, httpClient);

            eventHandler.EnsureTopicExists("me", topicNamespace, topicName);

            // no explicit assertion, because our "assertion" is "no exception thrown"

            msgHandler.Verify(x => x.Send(It.Is(getTopicRequestMessageArgument)), Times.Once);
            msgHandler.Verify(x => x.Send(It.Is(putTopicRequestMessageArgument)), Times.Once);
        }

        [Fact]
        public void PublishHasToPutCorrectRequest()
        {
            var topicNamespace = "namespace";
            var topicName = "topic";

            var msgHandler = new Mock<FakeHttpMessageHandler>() { CallBase = true };
            var httpClient = new HttpClient(msgHandler.Object);
            httpClient.BaseAddress = new Uri("http://test/");

            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK);
            httpResponseMessage.Content = new StringContent("OK");

            Expression<Func<HttpRequestMessage, bool>> requestMessageArgument = msg =>
                 msg.Method == HttpMethod.Put &&
                 msg.RequestUri.ToString() == $"http://test/{topicNamespace}/{topicName}/publish" &&
                 msg.Content != null &&
                 msg.Headers.Contains("owner-key");

            msgHandler.Setup(t => t.Send(It.Is(requestMessageArgument)))
                    .Returns(httpResponseMessage);

            Mock<IOptions<EventHandlerConfiguration>> configurationOptions = new Mock<IOptions<EventHandlerConfiguration>>();
            configurationOptions.SetupGet(x => x.Value).Returns(new EventHandlerConfiguration());

            var eventHandler = new Common.Events.EventHandler(configurationOptions.Object, httpClient);

            eventHandler.Publish("me", topicNamespace, topicName, "test");

            // no explicit assertion, because our "assertion" is "no exception thrown"

            msgHandler.Verify(x => x.Send(It.Is(requestMessageArgument)), Times.Once);
        }

        [Fact]
        public void testHandleExceptions()
        {
            var topicNamespace = "namespace";
            var topicName = "topic";

            var errorMessage = new Common.Events.Models.ErrorMessage()
            {
                Code = "400",
                Message = "This is an error"
            };

            var msgHandler = new Mock<FakeHttpMessageHandler>() { CallBase = true };
            var httpClient = new HttpClient(msgHandler.Object);
            httpClient.BaseAddress = new Uri("http://test/");

            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.BadRequest);
            httpResponseMessage.Content = new StringContent(JsonConvert.SerializeObject(errorMessage));

            Expression<Func<HttpRequestMessage, bool>> requestMessageArgument = msg =>
                 msg.Method == HttpMethod.Put &&
                 msg.RequestUri.ToString() == $"http://test/{topicNamespace}/{topicName}/publish" &&
                 msg.Content != null &&
                 msg.Headers.Contains("owner-key");

            msgHandler.Setup(t => t.Send(It.Is(requestMessageArgument)))
                    .Returns(httpResponseMessage);

            Mock<IOptions<EventHandlerConfiguration>> configurationOptions = new Mock<IOptions<EventHandlerConfiguration>>();
            configurationOptions.SetupGet(x => x.Value).Returns(new EventHandlerConfiguration());

            var eventHandler = new Common.Events.EventHandler(configurationOptions.Object, httpClient);

            Exception ex = Assert.Throws<Exception>(() => eventHandler.Publish("me", topicNamespace, topicName, "test"));

            Assert.Equal(errorMessage.Message, ex.Message);
            msgHandler.Verify(x => x.Send(It.Is(requestMessageArgument)), Times.Once);
        }

        [Fact]
        public void testHandleForbiddenExceptions()
        {
            var topicNamespace = "namespace";
            var topicName = "topic";

            var errorMessage = new Common.Events.Models.ErrorMessage()
            {
                Code = "400",
                Message = "You are forbidden to access this shit"
            };

            var msgHandler = new Mock<FakeHttpMessageHandler>() { CallBase = true };
            var httpClient = new HttpClient(msgHandler.Object);
            httpClient.BaseAddress = new Uri("http://test/");

            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.Forbidden);
            httpResponseMessage.Content = new StringContent(JsonConvert.SerializeObject(errorMessage));

            Expression<Func<HttpRequestMessage, bool>> requestMessageArgument = msg =>
                 msg.Method == HttpMethod.Put &&
                 msg.RequestUri.ToString() == $"http://test/{topicNamespace}/{topicName}/publish" &&
                 msg.Content != null &&
                 msg.Headers.Contains("owner-key");

            msgHandler.Setup(t => t.Send(It.Is(requestMessageArgument)))
                    .Returns(httpResponseMessage);

            Mock<IOptions<EventHandlerConfiguration>> configurationOptions = new Mock<IOptions<EventHandlerConfiguration>>();
            configurationOptions.SetupGet(x => x.Value).Returns(new EventHandlerConfiguration());

            var eventHandler = new Common.Events.EventHandler(configurationOptions.Object, httpClient);

            Exception ex = Assert.Throws<UnauthorizedAccessException>(() => eventHandler.Publish("me", topicNamespace, topicName, "test"));

            Assert.Equal(errorMessage.Message, ex.Message);
            msgHandler.Verify(x => x.Send(It.Is(requestMessageArgument)), Times.Once);
        }

        [Fact]
        public void TestSubscribeBodyMessage()
        {
            var pushConfig = new PushConfig { Url = "myUrl" };
            var subscriptionConfig = new SubscriptionConfig { Push = pushConfig };
            var subscription = new Subscription { Config = subscriptionConfig, Topic = "myTopic" };

            var jsonDes = JsonConvert.SerializeObject(subscription, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });

            Assert.Equal(@"{""topic"":""myTopic"",""config"":{""push"":{""url"":""myUrl""}}}", jsonDes);
        }
    }
}
